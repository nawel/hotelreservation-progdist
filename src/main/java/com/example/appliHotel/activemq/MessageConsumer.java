package com.example.appliHotel.activemq;

import com.example.appliHotel.entities.Room;
import com.example.appliHotel.entities.Suite;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class MessageConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(MessageConsumer.class);


    // écoute les messages de la queue
    @JmsListener(destination = "appliHotel-queue")
    public void messageListener(Suite suite) {
        LOGGER.info("Message received! {}", suite);
    }
}
