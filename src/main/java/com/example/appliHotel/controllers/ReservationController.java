package com.example.appliHotel.controllers;

import com.example.appliHotel.entities.DateDTO;
import com.example.appliHotel.entities.Reservation;
import com.example.appliHotel.services.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class ReservationController {

    @Autowired
    private ReservationService reservationService;


    //############################ CREATE ############################//

    @GetMapping("/reservation")
    public List<Reservation> getReservations() throws ParseException {
        return this.reservationService.getReservations();
    }
   /* @GetMapping(value="/reservation/{id}")
    public Reservation getReservation(@PathVariable(name="id") Long id){
        return this.reservationService.getReservation(id);
    }*/

    @GetMapping(value="/reservation/{username}")
    public Iterable<Reservation> getReservationByUser(@PathVariable(name="username") String username){
        return this.reservationService.getReservationByUser(username);
    }

    @PostMapping(value="/reservation")
    public Reservation addReservation (@RequestBody Reservation reservation){
        return this.reservationService.addReservation(reservation);
    }

    @PostMapping(value="/reservation/{username}/{idRoom}")
    public Reservation addReservationToUser (@PathVariable(name="username") String username, @PathVariable(name="idRoom") Long idRoom, @RequestBody DateDTO dates) throws ParseException {
        return this.reservationService.addReservation2(username,idRoom ,dates);
    }

    @PutMapping(value="/reservation/{id}")
    public Reservation updateReservation (@PathVariable(name="id") Long id, @RequestBody DateDTO dates) throws ParseException{
        return this.reservationService.updateReservation(id,dates);
    }

    @DeleteMapping(value="/reservation/{id}")
    public void deleteRoom (@PathVariable(name="id") Long id) {
       this.reservationService.deleteRoom(id);
    }



}
