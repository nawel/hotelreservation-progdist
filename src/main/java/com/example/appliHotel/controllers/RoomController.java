package com.example.appliHotel.controllers;

import com.example.appliHotel.entities.Room;
import com.example.appliHotel.entities.Suite;
import com.example.appliHotel.entities.Venue;
import com.example.appliHotel.repositories.RoomRepository;
import com.example.appliHotel.services.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*")
public class RoomController {

    @Autowired
    private RoomService roomService;
    @Autowired
    private RoomRepository roomRepository;

    @Autowired
    private JmsTemplate jmsTemplate;

    @GetMapping("/")
    public String hello(){
        return " hello test 2";
    }

//GET
    @GetMapping(value="/room/{id}")
    public Room getRoom(@PathVariable(name="id") Long id){
        return this.roomService.getRoom(id);
    }

    @GetMapping(value="/room/suite")
    public List<Room> getSuite(){
        return this.roomService.getSuite();
    }

    @GetMapping(value="/room/venue")
    public List<Room> getVenue(){
       return this.roomService.getVenue();
    }


    //CREATE
    /**@RequestMapping(method = RequestMethod.POST, value = "/room/suite", consumes= "application/json; charset=UTF-8" )
    public Room createSuite( @RequestBody Suite suite) throws Exception {
        return this.roomService.createSuite(suite);
    }**/
    @RequestMapping(method = RequestMethod.POST, value = "/room/venue", consumes= "application/json; charset=UTF-8" )
    public Room createVenue(@RequestBody Venue venue) throws Exception {
        return this.roomService.createVenue(venue);
    }

    @PostMapping("/room/suite")
    public ResponseEntity<String> addSuite(@RequestBody Suite suite) {
        try {
            roomService.createSuite(suite);
            jmsTemplate.convertAndSend("appliHotel-queue", suite);
            System.out.println(suite);
            System.out.println(suite.toString() + "Suite envoyée - nombre de chambre dans la db"+ ":" + " " + roomRepository.count());
            return new ResponseEntity<>("Message envoyé.", HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

//UPDATE
    @PutMapping(value="/room/suite/{id}")
    public Room updateSuite (@PathVariable(name="id") Long id, @RequestBody Suite suite){
        return roomService.updateSuite(id,suite);
    }
    @PutMapping(value="/room/venue/{id}")
    public Room updateVenue (@PathVariable(name="id") Long id, @RequestBody Venue venue){
        return roomService.updateVenue(id,venue);
    }
//DELETE
    @DeleteMapping(value="/room/{id}")
    public void deleteRoom (@PathVariable(name="id") Long id){
        this.roomService.deleteRoom(id);
    }


}
