package com.example.appliHotel;

import com.example.appliHotel.entities.*;
import com.example.appliHotel.repositories.ReservationRepository;
import com.example.appliHotel.repositories.RoomRepository;
import com.example.appliHotel.repositories.PersonRepository;

import com.example.appliHotel.services.PersonService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.text.SimpleDateFormat;
import java.util.Date;


@SpringBootApplication
public class AppliHotelApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppliHotelApplication.class, args);
	}


	@Bean
	public CommandLineRunner test (PersonService personService,PersonRepository personRepository, RoomRepository roomRepository, ReservationRepository reservationRespository){
		return (args)->{

			//CREATION DE ROLE
			Role admin = personService.save(new Role("ADMIN"));
			Role user = personService.save(new Role("USER"));

			// CREATION DES USERS

			Person adminUser = personService.createAdmin("admin", "admin", "admin", "admin", "0102030405", "admin@admin.com");
			// CREATION DES SUITES
			Room suite = new Suite(203, " suite parentale", "une suite avec vue sur l'Arc de Triomphe", 13, 1000, 3, true);
			roomRepository.save(suite);

			// CREATION DES USERS
			Person basicUser = personService.createUser("user", "user", "user", "user", "0102030405", "user@user.com");
			// CREATION DES VENUES
			Room venue = new Venue(10, " salle de conférence", "une grande salle pour conférence", 0, 2000, 500, SetupVenue.CONFERENCE, true);
			roomRepository.save(venue);
		};
	}

	@Bean
	BCryptPasswordEncoder getBCPE(){
		return new BCryptPasswordEncoder();
	}
}
