package com.example.appliHotel.services;

import com.example.appliHotel.entities.Room;
import com.example.appliHotel.entities.Suite;
import com.example.appliHotel.entities.Venue;
import com.example.appliHotel.repositories.RoomRepository;
import org.hibernate.ObjectDeletedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Service
public class RoomService {
        RoomRepository roomRepository;

        @Autowired
        public RoomService(RoomRepository roomRepository) {
            super();
            this.roomRepository = roomRepository;
        }

        public Room getRoom(@PathVariable(name="id") Long id){
            return roomRepository.findById(id).get();
        }

        public List<Room> getSuite(){
            List<Room> allRoom = roomRepository.findAll();
            List<Room> test = new ArrayList<>() ;
            for (Room r : allRoom) {
                System.out.println(r.getClass().getSimpleName());
                if (r.getClass().getSimpleName().equals("Suite")) {
                    test.add(r);
                } else { System.out.println("KO"); }
            }
            return test;
        }

        public List<Room> getVenue(){
            List<Room> allRoom = roomRepository.findAll();
            List<Room> test = new ArrayList<>() ;
            for (Room r : allRoom) {
                System.out.println(r.getClass().getSimpleName());
                if (r.getClass().getSimpleName().equals("Venue")) {
                    test.add(r);
                } else { System.out.println("KO");}
            }
            return test;
        }

 //CREATION DES ROOMS
        public Room createSuite( @RequestBody Suite suite) throws Exception {
            roomRepository.save(suite);
            return suite;
        }

        public Room createVenue( @RequestBody Venue venue) throws Exception {
            roomRepository.save(venue);
            return venue;
        }

        public Room updateSuite (@PathVariable(name="id") Long id, @RequestBody Suite s){
            s.setIdRoom(id);
            return roomRepository.save(s);
        }
        public Room updateVenue (@PathVariable(name="id") Long id, @RequestBody Venue v){
            v.setIdRoom(id);
            return roomRepository.save(v);
        }

        // ne peut pas être delete si elle est lié à une réservation -- on va dire qu'on peut pas supprimer de chambre qui a une réservation
        // ajouter un champs indisponible
        public void deleteRoom (@PathVariable(name="id") Long id){
               roomRepository.deleteById(id);
        }




}
