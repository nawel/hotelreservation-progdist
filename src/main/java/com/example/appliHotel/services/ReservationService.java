package com.example.appliHotel.services;

import com.example.appliHotel.entities.DateDTO;
import com.example.appliHotel.entities.Person;
import com.example.appliHotel.entities.Reservation;
import com.example.appliHotel.entities.Room;
import com.example.appliHotel.repositories.ReservationRepository;
import com.example.appliHotel.repositories.PersonRepository;
import com.example.appliHotel.repositories.RoomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.*;

import javax.transaction.Transactional;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;


@Service
@Transactional
public class ReservationService {


    ReservationRepository reservationRepository;
    PersonRepository personRepository;
    RoomRepository roomRepository;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository, PersonRepository personRepository, RoomRepository roomRepository) {
        super();
        this.reservationRepository = reservationRepository;
        this.personRepository = personRepository;
        this.roomRepository = roomRepository;
    }

    public List<Reservation> getReservations() throws ParseException {
        List<Reservation> reservationList = reservationRepository.findAll();
        return reservationList;
    }

    public Reservation getReservation(@PathVariable(name="id") Long id){
        return reservationRepository.findById(id).get();
    }


    public Iterable<Reservation> getReservationByUser(@PathVariable(name="username") String username){
        Person person = personRepository.findByUsername(username);
        return reservationRepository.findByPerson(person);
    }

    public Reservation addReservation (@RequestBody Reservation reservation){
        return reservationRepository.save(reservation);
    }

    public Reservation addReservation2 (@PathVariable(name="username") String username, @PathVariable(name="idRoom") Long idRoom, @RequestBody DateDTO dates) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Room room = roomRepository.findById(idRoom).get();
        Person person = personRepository.findByUsername(username);
        Date beginRent = dateFormat.parse(dates.getBeginRent());
        Date endRent = dateFormat.parse(dates.getEndRent());
        Reservation reservationAdd = new Reservation(beginRent, endRent, person, room);
        return reservationRepository.save(reservationAdd);
    }


    public Reservation updateReservation (@PathVariable(name="id") Long id,@RequestBody DateDTO dates) throws ParseException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date beginRent = dateFormat.parse(dates.getBeginRent());
        Date endRent = dateFormat.parse(dates.getEndRent());
        Reservation reservation =  this.getReservation(id);
        reservation.setBeginRent(beginRent);
        reservation.setEndRent(endRent);
        return reservationRepository.save(reservation);
    }

    public void deleteRoom (@PathVariable(name="id") Long id){
        reservationRepository.deleteById(id);
    }

}
