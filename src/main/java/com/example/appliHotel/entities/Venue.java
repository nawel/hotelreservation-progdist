package com.example.appliHotel.entities;


import javax.persistence.Entity;

@Entity
public class Venue extends Room{

    private int nbPerson;
    public SetupVenue setup ;
    private boolean withPodium;

    public Venue() {
        super();
    }

    public Venue(int roomNumber, String name, String description, int floor, double price, int nbPerson, SetupVenue setup, boolean withPodium) {
        super(roomNumber, name, description, floor, price);
        this.nbPerson = nbPerson;
        this.setup = setup;
        this.withPodium = withPodium;
    }

    public int getNbPerson() {
        return nbPerson;
    }

    public void setNbPerson(int nbPerson) {
        this.nbPerson = nbPerson;
    }

    public boolean isWithPodium() {
        return withPodium;
    }

    public void setWithPodium(boolean withPodium) {
        this.withPodium = withPodium;
    }

    public SetupVenue getSetup() {
        return setup;
    }

    public void setSetup(SetupVenue setup) {
        this.setup = setup;
    }
}
