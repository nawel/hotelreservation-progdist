package com.example.appliHotel.entities;

import javax.persistence.Entity;

@Entity
public class Suite extends Room {

    private int nbRoom;
    private boolean hasKitchen;


    public Suite(int roomNumber, String name, String description, int floor, double price, int nbRoom, boolean hasKitchen){
        super(roomNumber, name,description,floor,price);
        this.nbRoom=nbRoom;
        this.hasKitchen=hasKitchen;
    }

    public Suite(){
        super();
    }

    public int getNbRoom() {
        return nbRoom;
    }

    public void setNbRoom(int nbRoom) {
        this.nbRoom = nbRoom;
    }

    public boolean isHasKitchen() {
        return hasKitchen;
    }

    public void setHasKitchen(boolean hasKitchen) {
        this.hasKitchen = hasKitchen;
    }

    @Override
    public String toString() {
        return "Suite{" +
                "nbRoom=" + nbRoom +
                ", hasKitchen=" + hasKitchen +
                '}';
    }
}
