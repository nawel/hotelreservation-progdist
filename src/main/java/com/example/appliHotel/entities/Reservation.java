package com.example.appliHotel.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
//@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "idRent", scope = Long.class)
public class Reservation {

    public Long idRent;
    public Date beginRent;
    public Date endRent;
    public Person person;
    public Room room;

    public Reservation() {
        super();
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public Long getIdRent() {
        return idRent;
    }

    public void setIdRent(Long id_rent) {
        this.idRent = id_rent;
    }

    public Date getBeginRent() {
        return beginRent;
    }

    public void setBeginRent(Date beginRent) {
        this.beginRent = beginRent;
    }

    public Date getEndRent() {
        return endRent;
    }

    public void setEndRent(Date endRent) {
        this.endRent = endRent;
    }


    @ManyToOne(cascade = CascadeType.MERGE)
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @ManyToOne(cascade = CascadeType.MERGE)
    public Room getRoom() {
        return room;
    }
    public void setRoom(Room room) {
        this.room = room;
    }

    public Reservation(Date beginRent, Date endRent, Person person, Room room) {
        super();
        this.beginRent = beginRent;
        this.endRent = endRent;
        this.person = person;
        this.room = room;
    }
}
