package com.example.appliHotel.entities;

public class DateDTO {

    private String beginRent;
    private String endRent;

    public String getBeginRent() {
        return beginRent;
    }

    public void setBeginRent(String beginRent) {
        this.beginRent = beginRent;
    }

    public String getEndRent() {
        return endRent;
    }

    public void setEndRent(String endRent) {
        this.endRent = endRent;
    }
}
