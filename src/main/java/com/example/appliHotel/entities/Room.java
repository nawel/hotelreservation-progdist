package com.example.appliHotel.entities;

import com.fasterxml.jackson.annotation.*;
import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Room implements Serializable {

    private static final long serialVersionUID = 1L;
    private long idRoom;
    private int roomNumber;
    private String name;
    private String description;
    private int floor;
    private double price;
    private List<Reservation> reservationListRoom = new ArrayList<Reservation>();


    public Room() {
        super();
    }

    public Room(int roomNumber, String name, String description, int floor, double price) {
        this.roomNumber = roomNumber;
        this.name = name;
        this.description = description;
        this.floor = floor;
        this.price = price;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public long getIdRoom() {
        return idRoom;
    }

    public void setIdRoom(long id_room) {
        this.idRoom = id_room;
    }

    public int getRoomNumber() {
        return roomNumber;
    }

    public void setRoomNumber(int roomNumber) {
        this.roomNumber = roomNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public int getFloor() {
        return floor;
    }

    public void setFloor(int floor) {
        this.floor = floor;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @JsonIgnore
    @OneToMany(mappedBy = "room", cascade = CascadeType.MERGE, fetch = FetchType.EAGER)
    public List<Reservation> getReservationListRoom() {
        return reservationListRoom;
    }

    public void setReservationListRoom(List<Reservation> reservationListRoom) {
        this.reservationListRoom = reservationListRoom;
    }

    @Override
    public String toString() {
        return "Room{" +
                "idRoom=" + idRoom +
                ", roomNumber=" + roomNumber +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", floor=" + floor +
                ", price=" + price +
                ", reservationListRoom=" + reservationListRoom +
                '}';
    }
}
