package com.example.appliHotel.repositories;

import com.example.appliHotel.entities.Room;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface RoomRepository extends JpaRepository<Room, Long> {

    List<Room> findByRoomNumber(Long roomNumber);
    List<Room> findAll();

    long count();

}
