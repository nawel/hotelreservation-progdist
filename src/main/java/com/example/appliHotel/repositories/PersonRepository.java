package com.example.appliHotel.repositories;

import com.example.appliHotel.entities.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {
    List<Person> findAll();
    Person findByUsername(String username);
    //List<Person> findByAdminIsTrue();
}
