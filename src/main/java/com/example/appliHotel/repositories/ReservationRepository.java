package com.example.appliHotel.repositories;

import com.example.appliHotel.entities.Person;
import com.example.appliHotel.entities.Reservation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    List<Reservation> findAll();
    //Reservation findById(int reservationId);
    List<Reservation> findByPerson(Person person);
   // List<Reservation> findByRoom(Room room);

}
